const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.set('view engine', 'ejs');
// app.use( bodyParser.urlencoded( { extended: true } ) );
app.use(express.urlencoded({ extended: false }));

// allow parsing of data from a form

app.use(express.json());
app.use(express.static('public'));

let arrayItems = ['Buy Food', 'Cook Food', 'Eat Food'];
let workItems = [];
/* GET */
app.get('/', (req, res) => {
	const today = new Date();
	const currentDay = today.getDay();
	const options = {
		weekday: 'long',
		month: 'long',
		day: 'numeric',
	};
	const dateToday = today.toLocaleDateString('en-us', options);
	res.render('list', {
		currentDay: currentDay,
		title: dateToday,
		listItems: arrayItems,
	});
});
app.get('/work', (req, res) => {
	res.render('list', {
		currentDay: 5,
		title: 'Work List',
		listItems: workItems,
	});
});

app.get('/about', (req, res) => {
	res.render('about');
});

/* POST */
app.post('/', (req, res) => {
	const inputItem = req.body.inputItem;
	const buttonValue = req.body.button;
	if (buttonValue === 'Work') {
		workItems.push(inputItem);
		res.redirect('/work');
	} else {
		arrayItems.push(inputItem);
		res.redirect('/');
	}
});

app.listen(3000, () => {
	console.log('Listening to TODO port 3000');
});
