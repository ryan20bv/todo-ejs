// require
const express = require( "express" );
const _ = require( "lodash" );


//


// 
const app = express();
const port = 3000;


// use template ejs
app.set( 'view engine', 'ejs' );
app.use( express.static( 'public' ) );
// add body parsing using express for posting from form
app.use( express.urlencoded( { extended: true } ) );
app.use( express.json() );

const item1 = {detail: 'Welcome to TODO List'};
const item2 = {detail: 'Click + to add new todo'};
const item3 = {detail: '<=== Click checkbox to delete'};

const defaultItems = [item1, item2, item3];

let sideNavList = [ "TODAY" ];
// let items = [];
// let workItems = [];
let sideNavArray = [[]]

// function handleArray(){
// 	sideNavArray = sideNavList.map((list) => {
// 	return (list = new Array());
// });
// }



app.get( '/', ( req, res ) => {
	// handleArray()
	// console.log(sideNavArray);
	if (sideNavArray[0].length === 0) {
		sideNavArray[0].push(...defaultItems);
	}
	res.render( 'list', {title: "TODAY", listItems: sideNavArray[0], sideNavList:sideNavList });
} )

app.get( "/:category", ( req, res ) => {
	const category = _.toUpper( req.params.category )
	if ( category === "today" || category === "TODAY" ) {
		res.redirect("/");
	} else {
		const index = sideNavList.indexOf( category );
		// console.log( index )
	
		if ( sideNavArray[ index ].length === 0 ) {
			sideNavArray[index].push(...defaultItems)
		}

	// if (workItems.length === 0) {
	// 	workItems = [...defaultItems];
	// }
		// console.log( sideNavArray[ index ] );

	res.render("list", {
		title: category,
		listItems: sideNavArray[index],
		sideNavList: sideNavList,
	});
	}
	
})

app.post( '/', ( req, res ) => {
	// console.log(req.body.category);
	const category = _.lowerCase(req.body.category);
	const newItem = {
		detail: _.capitalize(req.body.inputItem),
	};
	const index = sideNavList.indexOf( req.body.category );
	sideNavArray[index].push( newItem );
	if (category === 'today' || category === "TODAY") {
		// items.push(newItem);
		res.redirect( '/' );
	} else {
		// workItems.push( newItem );
		res.redirect( `/${category}` );
	}
	
})

app.post( '/delete', ( req, res ) => {
	const checkboxDetail = req.body.checkbox;
	const hiddenCategory = _.lowerCase( req.body.hiddenCategory )
	const index = sideNavList.indexOf(req.body.hiddenCategory);

	const itemIndex = sideNavArray[ index ].findIndex( item => {
		return item.detail === checkboxDetail
	} )

	sideNavArray[ index ].splice( itemIndex, 1 );

	if (hiddenCategory === "today" || hiddenCategory === "TODAY") {
		// const itemIndex = items.findIndex( item => {
		// 	return item.detail === checkbox;
		// } )
		// items.splice( itemIndex, 1 );
		res.redirect("/");
	} else {
		// const itemIndex = workItems.findIndex((item) => {
		// 	return item.detail === checkbox;
		// });
		// workItems.splice(itemIndex, 1);
		res.redirect(`/${hiddenCategory}`);
	}
	// console.log("not found");
} )

app.post( '/addList', ( req, res ) => {
	const newList = _.upperCase( req.body.sideInput );
	sideNavList.push( newList )
	sideNavArray.push( [] );
	res.redirect( "/" );
})

app.listen( port, () => {
	console.log(`Listening to port ${port}`);
})





